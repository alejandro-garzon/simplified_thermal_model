# -*- coding: utf-8 -*-
"""
Created on Fri Apr 30 11:41:12 2021

@author: agarz
"""
import numpy as np
from numpy.linalg import norm
import matplotlib.pyplot as plt

import apendice_c as vf

def range_xy(P):
    # P is 3 x 3 matrix
    # each row has the coordinates of a point
    # First column has the x-coordinates of all three points
    # Second column has the y-coordinates of all three points
    # Third column has the z-coordinates of all three points
    xp = P[:,0]
    yp = P[:,1]
    x = [np.min(xp), np.max(xp)]
    y = [np.min(yp), np.max(yp)]

    return x, y

def range_yz(P):
    # P is 3 x 3 matrix
    # each row has the coordinates of a point
    # First column has the x-coordinates of all three points
    # Second column has the y-coordinates of all three points
    # Third column has the z-coordinates of all three points
    yp = P[:,1]
    zp = P[:,2]
    y = [np.min(yp), np.max(yp)]
    z = [np.min(zp), np.max(zp)]

    return y, z


    

front  = [ [ 10, 10, 30 ], [ 10, 0, 30 ], [ 0, 0, 30 ] ]
bottom = [ [ 0, 10, 0 ], [ 0, 10, 30 ], [ 0, 0, 30 ] ]
left   = [ [ 0, 0, 30 ], [ 10, 0, 30 ], [ 10, 0, 0 ] ]
top    = [ [ 10, 10, 0 ], [ 10, 0, 0 ], [ 10, 0, 30] ]
right  = [ [ 0, 10, 0], [ 10, 10, 0], [ 10, 10, 30 ] ]
rear   = [ [ 10, 0, 0 ], [ 10, 10, 0 ], [ 0, 10, 0 ] ]

faces = np.array([ front, bottom, left, top, right, rear ])

start_point = faces[:,0,:]
mid_point = faces[:,1,:]
end_point = faces[:,2,:]

ex = np.zeros((6,3))
ey = np.zeros((6,3))
ez = np.zeros((6,3))

Lx =np.zeros(6)
Ly =np.zeros(6)

# Assign local coordinate axes to each face
for i in range(6):
    mp = mid_point[i]
    
    u = end_point[i] - mp
    r = norm(u)
    ex[i] = u/r
    Lx[i] = r
    
    v = start_point[i] - mp
    r = norm(v)
    ey[i] = v/r
    Ly[i] =r
    
    w = np.cross(u,v)
    ez[i] = w/norm(w)

nsub = 8 # number of subdivisions of shorter edges
Lshort=10
dL = Lshort/nsub
nx = (Lx/dL).astype(int)
ny = (Ly/dL).astype(int)
nn = nx*ny

ib = np.cumsum(nn)
ia = np.concatenate( ( [0], ib[:-1] ) )
ntot = ib[-1]
sub_points = np.zeros( (ntot, 3, 3) )


# Generating list of coordinates triples for each subdivision
for i in range(6):
    #print("============ %d =============" % i)
    mp = mid_point[i]
    exi = ex[i]
    eyi = ey[i]
    iai = ia[i]
    nxi = nx[i]
    nyi = ny[i]
    for ky in range(nyi):
        for kx in range(nxi):
            p0 = mp + kx*dL*exi + ky*dL*eyi
            px = p0 + dL*exi
            py = p0 + dL*eyi
            
            k = ky*nxi + kx
            #print(iai + k)
            sub_points[ iai + k ] = np.array([p0,px,py])
            #sub_points = np.array([p0,px,py])
            #print('------------------------------')
            #print(sub_points[iai + k])


umbral = 1e-12

F = np.zeros((ntot,ntot))

np_i = np.zeros((3,3)) # coordinates of i-th face points on new axes
np_j = np.zeros((3,3)) # coordiants of j-th face points on new axes
#i = 0
for i in range(6):
    ez_i = ez[i]
    P = np.column_stack( ( ex[i], ey[i], ez_i ) )
    Pm = np.linalg.inv(P)
    
    mpi = mid_point[i]
    p_i = faces[i]

    iai = ia[i]
    
    for j in range(6):
        iaj =ia[j]
        
        if j != i:
            dot = np.dot(ez_i,ez[j])
            #print(dot)
            if abs(1+dot) < umbral: # faces are parallel
                #print("Faces are parallel")
                # change vectors to local coordinate system
                # of i-the face, with origin at mid_point[i]
                z = Pm.dot(start_point[j]-mpi)[2]

                for k in range(nn[i]):
                    kg = iai + k # global index
                    #print(kg)
                    pk = Pm.dot( ( sub_points[kg] - mpi ).T ).T
                    for el in range(nn[j]):
                        elg = iaj + el # global index
                        #print(elg)
                        pel = Pm.dot( ( sub_points[elg] -mpi ).T ).T

                        x, y = range_xy(pk)
                        xi, eta = range_xy(pel)
                        F[kg,elg] = vf.ViewFactorParalelo(x, y, xi, eta, z)
                        #print( "(kg,elg,F)=(%d,%d,%f)" % (kg, elg, F[kg,elg]) )

            elif abs(dot) < umbral: # faces are perpendicular
                #print("Faces are perpendicular")
                # Find line of intersection of the two planes
                ez_j = ez[j]
                ec = np.cross(ez_i, ez_j) # direction of line of intersection

                # finding how points of i-th face project on line of intersection
                pproj = p_i.dot(ec)
                # new origin will be the projection onto the line of instersection
                # of the point of i-th face with the smallest dot product
                k_min = np.argmin( pproj  )
                
                M = np.row_stack( ( ez_i, ez_j, ec ) )
                b0 = mpi.dot(ez_i)
                b1 = mid_point[j].dot(ez_j)
                b2 = pproj[k_min]
                b = np.array([b0, b1, b2])
                # find new origin, on line of intersection
                p0 = np.linalg.solve(M,b)

                ex_l = np.cross( ec, ez_i )
                Q = np.column_stack( (ex_l, ec, ez_i ) )
                Qm = np.linalg.inv(Q)

                # change vectors to local coordinate system
                # with origin at p0, on line of intersection
                for k in range(nn[i]):
                    kg = iai + k # global index
                    #print(kg)
                    pk = Qm.dot( ( sub_points[kg] - p0 ).T ).T
                    x, y = range_xy(pk)
                    
                    for el in range(nn[j]):
                        elg = iaj + el # global index
                        #print(elg)
                        pel = Qm.dot( ( sub_points[elg] - p0 ).T ).T
                        eta, xi = range_yz(pel)
                        
                        if abs(xi[0]) < 1e-12:
                            xi[0] = 1e-6

                        F[kg,elg] = vf.ViewFactorPerpendicular(x, y, xi, eta)
                        #print( "(kg,elg,F)=(%d,%d,%f)" % (kg, elg, F[kg,elg]) )
            else: # unknown case
                print("Unknown case")

np.savez('view_factor_matrix_sub_'+str(nsub), nsub=nsub, F=F, ia=ia, ib=ib, nn=nn)


plt.imshow(F)
