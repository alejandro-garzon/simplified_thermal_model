import numpy as np
from numpy import cos, sin
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.io import loadmat
import carcasa_conduction_3 as carcasa_conduction
import carcasa_radiation

pi = np.pi

def set_parameters(param):
    #global S2, Q1, Q2, Q3, Q4, Q5, w
    #global alpha1, epsilon
    global w, alb_interp, alpha1, area, dot, S, Ialb, epsilon, sigma
    global Qe, GLp, C, GRp # Aeff

    S2 = param['S2']
    Q1 = param['Q1']
    Q2 = param['Q2']
    Q3 = param['Q3']
    Q4 = param['Q4']
    Q5 = param['Q5']
    #Q6 = param['Q6']
    w = param['w']

    ncell=6 # Number of solar cells on each large face.
    Ac=30.18*ncell #source technical azur-space support TJ Solar Cell 3G28C
    At=10*30
    fc=Ac/At

    eta=0.28 #efficiency
    alphac=0.91-eta # net absorptivity solar cells
    alphap=0.5 # abosrptivity "shiny"
    # Effective absortivity faces with solar cells
    alphaq0=(fc*alphac)+((1-fc)*alphap) 
    #alphaq=round_significant(alphaq0,3) 
    alphaq=round(1000*alphaq0)/1000

    epsilonc=0.89 #emissivity solar cells
    epsilonp=0.05 # emissivity "shiny"
    # Effective emissivity faces with solar cells
    epsilonq0=(fc*epsilonc)+((1-fc)*epsilonp)
    #epsilonq=round_significant(epsilonq0,3)
    epsilonq=round(1000*epsilonq0)/1000

    #global alpha1 epsilon
    alpha1 = np.zeros(6)
    alpha1[0]=alphap #back
    alpha1[1]=alphap #earth
    alpha1[2]=alphap #front
    alpha1[3]=alphaq #left
    alpha1[4]=alphaq #right
    alpha1[5]=alphaq #space

    epsilon = np.zeros(6)
    epsilon[0]=epsilonp
    epsilon[1]=epsilonp
    epsilon[2]=epsilonp
    epsilon[3]=epsilonq
    epsilon[4]=epsilonq
    epsilon[5]=epsilonq
        
    area = np.zeros(6)
    area[0]=0.01
    area[1]=0.03
    area[2]=0.01
    area[3]=0.03
    area[4]=0.03
    area[5]=0.03

    # Below, for calculation see script infra_view_factors.m
    fA=0.228102
    fB=0.804692
    F = np.zeros(6)
    F[0]=fA
    F[1]=fB
    F[2]=fA
    F[3]=fA
    F[4]=fA
    F[5]=0

    S=1367 # W/m^2, Solar constant
    # Below, source Anderson, Justus and Watts, 2001, Pag. 17, 7.
    # For conversion from surface at Top of Atmosphere (TOA) to
    # surface of Earth, see script radiosity_earth.m
    I=213 # W/m^2, Infrared radiosity
    alb=0.273 # nondimensional, Albedo coefficient of reflection
    Ialb=alb*S
    C=921.6 # J/K, Satellite heat capacity
             # This is the heat capacity of 1 kg of aluminum
    sigma=5.670373e-8 # Stefan-Boltzmann constant NIST
                       # CODATA recommended values 2010.

    #solar vector dot product
    dot = np.zeros(6)
    # in particular
    #dot[3]=0
    #dot[4]=0


    # Create list of interpolants of albedo integrals
    alb_interp = []
    alb_interp.append(interp1d(S2,Q1))
    alb_interp.append(interp1d(S2,Q2))
    alb_interp.append(interp1d(S2,Q3))
    alb_interp.append(interp1d(S2,Q4))
    alb_interp.append(interp1d(S2,Q5))
    alb_interp.append(lambda x:0)
    #alb_interp.append(interp1d(S2,Q6))
    # AL(2)=interp1d(S2,Q2)
    # AL(1)=interp1d(S2,Q1)
    # AL(3)=interp1d(S2,Q3)
    # AL(4)=interp1d(S2,Q4)
    # AL(5)=interp1d(S2,Q5)
    # AL(6)=0
    Qe = epsilon*area*F*I
    #Aeff = np.sum(epsilon*area)
    GL=carcasa_conduction.GL
    GL_row = np.sum(GL,axis=1)
    GLp = GL - np.diag(GL_row)
    
    C=carcasa_conduction.C

    # matrix of Gebhart factors
    B=carcasa_radiation.B
    e_internal = carcasa_radiation.epsilon
    B1=B.copy()
    np.fill_diagonal(B1,B1.diagonal()-1)
    GRp = sigma*((e_internal*area)[:,np.newaxis])*B1
    #print(B)
    #print(B1)
    #print(GRp)
    
def dTdt(t,T,inter):
    angle = (w*t) % (2*pi)

    if inter==1:
        dot[5]=cos(angle)
        dot[2]=0
        dot[1]=0
        dot[0]=sin(angle)
    elif inter==2:
        dot[5]=0
        dot[2]=0
        dot[1]=-cos(angle)
        dot[0]=sin(angle)
    elif inter==3:
        dot[:]=0
    elif inter==4:
        dot[5]=0
        dot[2]=-sin(angle)
        dot[1]=-cos(angle)
        dot[0]=0
    elif inter==5:
        dot[5]=cos(angle)
        dot[2]=-sin(angle)
        dot[1]=0
        dot[0]=0
    else:
        dot[:]=np.nan
        print("Invalid interval number")
        
    F_alb = [fun(angle) for fun in alb_interp]
    #print(F_alb)
    Qs = alpha1*area*dot*S
    Qalb = alpha1*area*F_alb*Ialb
    Qst = epsilon*area*sigma*T**4

    cond = GLp.dot(T)
    rad = GRp.dot(T**4)
    # print(Qs)
    # print(Qalb)
    # print(Qe)
    # print(Qst)
    #print('cond=',cond)
    #print('rad=',rad)
    # print("T=",T)
    # print("-------------")
    #dT_dt = (cond + Qs + Qalb + Qe - Qst)/C
    dT_dt = (cond + rad + Qs + Qalb + Qe - Qst)/C
    return dT_dt

"""
if __name__ == "__main__":

    mat = loadmat("earth.mat")
    S2 = mat['S2'].flatten()
    Q2 = mat['Q2'].flatten()

    plt.plot(S2,Q2)

    mat = loadmat("back.mat")
    Q1 = mat['Q1'].flatten()

    mat = loadmat("front.mat")
    Q3 = mat['Q3'].flatten()

    mat = loadmat("left.mat")
    Q4 = mat['Q4'].flatten()

    mat = loadmat("right.mat")
    Q5 = mat['Q5'].flatten()

    param = {}
    param['S2'] = S2
    param['Q1'] = Q1
    param['Q2'] = Q2
    param['Q3'] = Q3
    param['Q4'] = Q4
    param['Q5'] = Q5
    #param['Q6'] = Q6

    set_parameters(param)
    #set_parameters(0)
    print(alb_interp)
    dTdt(0,0,1)
 
"""
