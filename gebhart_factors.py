# -*- coding: utf-8 -*-
"""
Created on Mon May 10 17:10:33 2021

@author: agarz
"""
import numpy as np
import matplotlib.pyplot as plt

file=np.load("view_factor_matrix.npz")
F=file['F']

nf=6

val = 0.5
epsilon = val*np.ones(nf)

# below, colum F[:,i] is multiplied by epsilon[i]
rho = 1-epsilon
alpha = F*rho

ix = np.arange(nf)
alpha1 = alpha.copy()
alpha1[ix,ix] = alpha[ix,ix]-1

B = np.zeros((nf,nf))

eF = F*epsilon

for i in range(nf):
    B[:,i]=np.linalg.solve(alpha1,-eF[:,i])

# check summation    
sum_check=np.sum(B,axis=1)
print(sum_check)
# check reciprocity
area = np.array([0.01, 0.03, 0.03, 0.03, 0.03, 0.01])

# below, row B[i,:] is multiplied by epsilon[i]*area[i]
BeA=B*((epsilon*area)[:,np.newaxis])
BB = BeA.T-BeA
print(np.max(BB))

np.savez('gebhart_factor_matrix',B=B,epsilon=epsilon)

if __name__ == "__main__":
    sigma=5.670373e-8
    T0=295
    
    GR_lin = 4*BeA*sigma*T0**3