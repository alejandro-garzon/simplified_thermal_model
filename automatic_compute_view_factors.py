# -*- coding: utf-8 -*-
"""
Created on Fri Apr 30 11:41:12 2021

@author: agarz
"""
import numpy as np
from numpy.linalg import norm
import apendice_c as vf

front  = [ [ 10, 10, 30 ], [ 10, 0, 30 ], [ 0, 0, 30 ] ]
bottom = [ [ 0, 10, 0 ], [ 0, 10, 30 ], [ 0, 0, 30 ] ]
left   = [ [ 0, 0, 30 ], [ 10, 0, 30 ], [ 10, 0, 0 ] ]
top    = [ [ 10, 10, 0 ], [ 10, 0, 0 ], [ 10, 0, 30] ]
right  = [ [ 0, 10, 0], [ 10, 10, 0], [ 10, 10, 30 ] ]
rear   = [ [ 10, 0, 0 ], [ 10, 10, 0 ], [ 0, 10, 0 ] ]

faces = np.array([ front, bottom, left, top, right, rear ])

start_point = faces[:,0,:]
mid_point = faces[:,1,:]
end_point = faces[:,2,:]

ex = np.zeros((6,3))
ey = np.zeros((6,3))
ez = np.zeros((6,3))

Lx =np.zeros(6)
Ly =np.zeros(6)

# Assign local coordinate axes to each face
for i in range(6):
    mp = mid_point[i]
    
    u = end_point[i] - mp
    r = norm(u)
    ex[i] = u/r
    Lx[i] = r
    
    v = start_point[i] - mp
    r = norm(v)
    ey[i] = v/r
    Ly[i] =r
    
    w = np.cross(u,v)
    ez[i] = w/norm(w)

umbral = 1e-12

F = np.zeros((6,6))

np_i = np.zeros((3,3)) # coordinates of i-th face points on new axes
np_j = np.zeros((3,3)) # coordiants of j-th face points on new axes
#i = 0
for i in range(6):
    ez_i = ez[i]
    P = np.column_stack( ( ex[i], ey[i], ez_i ) )
    Pm = np.linalg.inv(P)
    
    mpi = mid_point[i]
    p_i = faces[i]
    
    for j in range(6):
        if j != i:
            dot = np.dot(ez_i,ez[j])
            print(dot)
            if abs(1+dot) < umbral: # faces are parallel
                print("Faces are parallel")
                # change vectors to local coordinate system
                # with origin at mid_point[i]
                np_j[0] = Pm.dot(start_point[j]-mpi)
                np_j[1] = Pm.dot(mid_point[j]-mpi)
                np_j[2] = Pm.dot(end_point[j]-mpi)

                z = np_j[0,2]
                print(z)

                print(np_j)

                xp = np_j[:,0]
                yp = np_j[:,1]
                xi = [ np.min(xp), np.max(xp) ]
                eta = [ np.min(yp), np.max(yp) ]
                print(xi)
                print(eta)

                x = [0,Lx[i]]
                y = [0,Ly[i]]

                F[i,j] = vf.ViewFactorParalelo(x, y, xi, eta, z)
                
            elif abs(dot) < umbral: # faces are perpendicular
                print("Faces are perpendicular")
                # Find line of intersection of the two planes
                ez_j = ez[j]
                ec = np.cross(ez_i, ez_j) # direction of line of intersection

                # finding how points of i-th face project on line of intersection
                pproj = p_i.dot(ec)
                # new origin will be the projection onto the line of instersection
                # of the point of i-th face with the smallest dot product
                k_min = np.argmin( pproj  )
                
                M = np.row_stack( ( ez_i, ez_j, ec ) )
                b0 = mpi.dot(ez_i)
                b1 = mid_point[j].dot(ez_j)
                b2 = pproj[k_min]
                b = np.array([b0, b1, b2])
                # find new origin, on line of intersection
                p0 = np.linalg.solve(M,b)

                ex_l = np.cross( ec, ez_i )
                Q = np.column_stack( (ex_l, ec, ez_i ) )
                Qm = np.linalg.inv(Q)

                # change vectors to local coordinate system
                # with origin at p0, on line of intersection
                np_i[0] = Qm.dot(p_i[0]-p0)
                np_i[1] = Qm.dot(p_i[1]-p0)
                np_i[2] = Qm.dot(p_i[2]-p0)
                
                xp = np_i[:,0]
                yp = np_i[:,1]

                x = [np.min(xp), np.max(xp)]
                if abs(x[0]) < 1e-12:
                    x[0] = 1e-6
                    
                y = [np.min(yp), np.max(yp)]
                
                np_j[0] = Qm.dot(start_point[j]-p0)
                np_j[1] = Qm.dot(mid_point[j]-p0)
                np_j[2] = Qm.dot(end_point[j]-p0)

                etap = np_j[:,1]
                xip = np_j[:,2]

                eta = [ np.min(etap), np.max(etap) ]
                xi = [ np.min(xip), np.max(xip) ]
                if abs(xi[0]) < 1e-12:
                    xi[0] = 1e-6

                F[i,j] = vf.ViewFactorPerpendicular(x, y, xi, eta)
                
            else: # unknown case
                print("Unknown case")

np.savez('view_factor_matrix',F=F)
