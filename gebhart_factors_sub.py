# -*- coding: utf-8 -*-
"""
Created on Mon May 10 17:10:33 2021

@author: agarz
"""
import numpy as np
import matplotlib.pyplot as plt

file=np.load("view_factor_matrix_sub_8.npz")
F=file['F']

nf = F.shape[0]
#nf=6

val = 0.5
epsilon = val*np.ones(nf)

# below, colum F[:,i] is multiplied by epsilon[i]
rho = 1-epsilon
alpha = F*rho


ix = np.arange(nf)
alpha1 = alpha.copy()
alpha1[ix,ix] = alpha[ix,ix]-1


B = np.zeros((nf,nf))

eF = F*epsilon

for i in range(nf):
    B[:,i]=np.linalg.solve(alpha1,-eF[:,i])

# check summation    
sum_check=np.sum(B,axis=1)
#print(sum_check)
#print( "max=%f" % np.max(sum_check) )
#print( "min=%f" % np.min(sum_check) )
# check reciprocity
# In this case the reciprocity relation reduces to
# the matrix B begin symmetric
dB = B-B.T
#print(np.max(dB))

Bred = np.zeros((6,6))

ia=file['ia']
ib=file['ib']
nn=file['nn']

for i in range(6):
    fac =1/nn[i]
    for j in range(6):
        Bred[i,j] = fac*np.sum( B[ ia[i]:ib[i], ia[j]:ib[j] ] )

nsub=file['nsub']
np.savez('Bred_orden_GV_'+str(nsub),Bred=Bred)
# Changing indices
front, bottom, left, top, right, rear = list(range(6))
jx = np.array( [front, rear, top, right, bottom, left] )
ix = jx[:,np.newaxis]

Bred = Bred[ix,jx]
nsub=file['nsub']
np.savez('Bred_'+str(nsub),Bred=Bred)
print(Bred)