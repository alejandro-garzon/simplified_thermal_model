import numpy as np
	
def B(x,y,eta,xi,z):
    u = x - xi
    nu = y - eta
    p = np.sqrt(u**2+z**2)
    q = np.sqrt(nu**2+z**2)
    return nu*p*np.arctan(nu/p) + u*q*np.arctan(u/q) - ((z**2)/2)*np.log(u**2+nu**2+z**2)
	
def ViewFactorParalelo(x,y,xi,eta,z):
    sum =0
    for ele in range(2):
        for k in range(2):
            for j in range(2):
                for i in range(2):
                    term=(-1)**(i+j+k+ele)*B(x[i],y[j],eta[k],xi[ele],z)
                    sum += term
    A1 = (x[1]-x[0])*(y[1]-y[0])
    F12 = 1/(2*np.pi*A1)*sum
    return F12
	
def G(x,y,xi,eta):
    C = np.sqrt((x**2) + (xi**2))
    D = (y - eta)/C
    return (y - eta)*C*np.arctan(D) - C**2/4*(1-D**2)*np.log((C**2)*(1+D**2))
	
def ViewFactorPerpendicular(x,y,xi,eta):
    sum=0
    for ele in range(2):
        for k in range(2):
            for j in range(2):
                for i in range(2):
                    term=((-1)**(i+j+k+ele))*(G(x[i],y[j],xi[k],eta[ele]))
                    sum += term
    A1 = (x[1]-x[0])*(y[1]-y[0])
    F12 = (1/(2*np.pi*A1))*sum
    return F12
