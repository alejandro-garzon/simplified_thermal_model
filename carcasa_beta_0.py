import numpy as np
from scipy.io import loadmat
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt

import carcasa

pi=np.pi

mat = loadmat("earth.mat")
S2 = mat['S2'].flatten()
Q2 = mat['Q2'].flatten()

plt.figure(1)
plt.plot(S2,Q2)

mat = loadmat("back.mat")
Q1 = mat['Q1'].flatten()

mat = loadmat("front.mat")
Q3 = mat['Q3'].flatten()

mat = loadmat("left.mat")
Q4 = mat['Q4'].flatten()

mat = loadmat("right.mat")
Q5 = mat['Q5'].flatten()

# below, Gravitational parameter mu=GM
# from D.A. Vallado, 
# Fundamentals of Astrodynamics and Applications, 1997
# pag. 878, Table D-4.
mu=3.986004415e5# km^3/s^2 
Ro=7110
# below, angular velocity given by Kepler's third law.
w=np.sqrt(mu/Ro**3) # s^{-1}, units of time will be seconds
#w=0.001053087*60

Rt=6378
#Rtoa=Rt+30
sha=np.arccos(Rt/Ro)
P=2*pi/w

theta1=pi/2
theta2=(pi/2)+sha
theta3=(3*pi/2)-sha
theta4=3*pi/2
t0=0
t1=theta1/w
t2=theta2/w
t3=theta3/w
t4=theta4/w
tc = [t0,t1,t2,t3,t4,P]

param = {}
param['S2'] = S2
param['Q1'] = Q1
param['Q2'] = Q2
param['Q3'] = Q3
param['Q4'] = Q4
param['Q5'] = Q5

param['w'] = w

carcasa.set_parameters(param)

# creating list of times on which the solution will be
# plotted
s_t = 10 # suggested sample time
times = []
for i in range(5):
    duration = tc[i+1]-tc[i]
    nt = int(round(duration/s_t))
    ts = np.linspace(tc[i],tc[i+1],nt)    
    times.append(ts)
    

T0 = (273.15)*np.ones(6)
dT_dt = carcasa.dTdt(0,T0+10,1)
print(dT_dt)
nP = 10#100
tt = []#np.array([])
Tt = []

plt.figure(2)
plt.clf()
for i in range(nP):# Each iteration corresponds to one period P
    r = i*P + tc
    
    # loop over intervals on which the vector field is smooth
    for k in range(5):
        interval = k+1
        fun = lambda t,T: carcasa.dTdt(t,T,interval)
        t_eval = i*P + times[k]
        sol = solve_ivp(fun,(r[k],r[k+1]),T0, t_eval=t_eval,
                        rtol=1e-6, atol=1e-4)
        #plt.plot(sol.t,sol.y[0]-273.15)
        #tt = np.concatenate( ( tt, sol.t ) )
        #Tt = np.concatenate( ( Tt, sol.y[0] ), axis=1 )
        T0 = sol.y[:,-1]
        tt.append(sol.t)
        Tt.append(sol.y)

tt = np.concatenate(tt)
Tt = np.concatenate(Tt, axis=1)

# node labels
labels = ["back","earth","front","left","right","space"]
fstr = ["-", "--", "-."]
for i in range(6):
    plt.plot(tt/3600,Tt[i]-273.15, fstr[i%3], label=("node %d, " % (i))+labels[i] )
    #plt.plot(tt/3600,Tt[i]-273.15, label=("node %d, " % (i))+labels[i] )
plt.legend()
plt.xlabel('t (hour)')
plt.ylabel('T (Celsius deg.)')
#plt.ylim(-30,15)
plt.grid()

#np.savez('only_conduction',tt=tt,Tt=Tt)
np.savez('conduction_and_rad',tt=tt,Tt=Tt)




