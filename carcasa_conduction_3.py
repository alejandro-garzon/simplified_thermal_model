import numpy as np
# Computing conductance matrix and
# heat capacities
#Grosor de las l�minas
Grosor=0.002

# Longitudes Tapas
Largo_tapa = 0.1
Ancho_tapa = 0.1
Volumen_tapa=Largo_tapa*Ancho_tapa*Grosor
Area_trans=Grosor*Largo_tapa


# Longitudes caras
Largo_cara = 0.3
Ancho_cara = 0.1
Volumen_cara = Largo_cara*Ancho_cara*Grosor
Area_trans_cc =Grosor*Largo_cara

# Aluminum
rho = 2700 # density
cp = 900 # specific head
k = 200 # thermal conductivity
#alpha = 0.5 # apbsortivity
#S=1367 # solar constant
#epsilon=0.05 # emissivity
#sigma=5.670373E-8 # Stefan-Boltzmann constant

# Capacidad calor�fica de las tapas

C_tapa = cp*rho*Volumen_tapa
C_cara=cp*rho*Volumen_cara

C=np.array([C_tapa,C_tapa,C_cara,C_cara,C_cara,C_cara])

K_tc = k*Area_trans/(Largo_tapa/2)
K_ct = k*Area_trans/(Largo_cara/2)
K_cc = k*Area_trans_cc/(Ancho_cara/2)

KA = 1/(1/K_tc+1/K_ct) # net conductance between tapa and cara
KB = 1/(1/K_cc+1/K_cc) # net conductance between cara and cara

GL = np.zeros((6,6))

###################################################
# Indexing in Matlab script for Garzon-Villanueva (therm2.m)
# different from indexing in paper Garzon-Villanueva

jb = 0 # back
je = 1 # earth
jf = 2 # front
jl = 3 # left
jr = 4 # right
js = 5 # space
#---------------------------------------------

#respecto a back (tapa)
GL[jb,je] = KA #1/(1/K_tc+1/K_ct)
GL[jb,jr] = KA #GL[jb,je]
GL[jb,js] = KA #GL[jb,je]
GL[jb,jl] = KA #GL[jb,je]

#respecto a front (tapa)
GL[jf,je] = KA #1/(1/K_tc+1/K_ct)
GL[jf,jr] = KA #GL[jb,je]
GL[jf,js] = KA #GL[jb,je]
GL[jf,jl] = KA #GL[jb,je]

#respecto a earth (cara)
GL[je,jb] = KA #GL[jb,je]
GL[je,jf] = KA #GL[jf,je]
GL[je,jl] = KB #1/(1/K_cc+1/K_cc)
GL[je,jr] = KB #GL[je,jl]

#respecto a right (cara)
GL[jr,jb] = KA #GL[jb,jr]
GL[jr,jf] = KA #GL[jf,jr]
GL[jr,je] = KB #GL[je,jr]
GL[jr,js] = KB #1/(1/K_cc+1/K_cc)

#respecto a space (cara)
GL[js,jb] = KA #GL[jb,js]
GL[js,jf] = KA #GL[jf,js]
GL[js,jr] = KB #GL[jr,js]
GL[js,jl] = KB #1/(1/K_cc+1/K_cc)

#respecto a left (cara)
GL[jl,jb] = KA #GL[jb,jl]
GL[jl,jf] = KA #GL[jf,jl]
GL[jl,je] = KB #GL[je,jl]
GL[jl,js] = KB #1/(1/K_cc+1/K_cc)
