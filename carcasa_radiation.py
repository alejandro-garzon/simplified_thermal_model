# -*- coding: utf-8 -*-
"""
Created on Tue May 11 14:15:17 2021

@author: agarz
"""
import numpy as np

file=np.load('Bred_orden_GV_8.npz')
B = file['Bred']
epsilon = np.zeros(6)
#epsilon = file['epsilon']

# changing indexation
ix =np.zeros(6, dtype=int)

# new index (Matlab script indexation) is number in square bracket
# old index (Garzon-Villanueva, tesis Daniel) is number on the right-hand side

ix[0] = 5 # back (rear)
ix[1] = 1 # earth (bottom)
ix[2] = 0 # front
ix[3] = 2 # left
ix[4] = 4 # right
ix[5] = 3 # space (top)

#print(B)
B = B[ix[:,np.newaxis],ix]
#print('\n')
#print(B)
epsilon = epsilon[ix]
